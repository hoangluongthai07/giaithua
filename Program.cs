﻿using System;

class Program {
  static string playerName = ""; 
  static int guessCount = 0;
  static int bestGuessCount = int.MaxValue;

  static void Main(string[] args) {

    while (true) {
    Console.OutputEncoding = System.Text.Encoding.UTF8;
      Console.WriteLine("Menu:");
      Console.WriteLine("1. Chơi mới");
      Console.WriteLine("2. Xem kỷ lục");
      Console.WriteLine("3. Thoát");
      
      int choice = Convert.ToInt32(Console.ReadLine());
      
      switch (choice) {
        case 1:
          PlayGame();
          break;
        case 2:
          if (playerName != "") {
            Console.WriteLine("Kỷ lục ít nhất số lần đoán: " + bestGuessCount  
                              + " của người chơi " + playerName);
          }
          else {
            Console.WriteLine("Chưa có kỷ lục");
          }
          break;
        case 3:
          Environment.Exit(0);
          break;
      }
    }
  }

  static void PlayGame() {
    
    guessCount = 0;
    int number = new Random().Next(0, 100); 
    int guess;
    
    do {
      Console.Write("Đoán số: ");
      guess = Convert.ToInt32(Console.ReadLine());
            
      if (guess > number) {
        Console.WriteLine("Số bạn đoán lớn hơn");
      }
      else if (guess < number) {
        Console.WriteLine("Số bạn đoán nhỏ hơn");
      }
      else {
        Console.WriteLine("Chúc mừng, bạn đã đoán đúng!");
        break;
      } 
      guessCount++;
      
    } while (guess != number);

    
    if (guessCount < bestGuessCount) {
      Console.Write("Bạn lập kỷ lục mới! Nhập tên: ");
      playerName = Console.ReadLine();
      bestGuessCount = guessCount;
    }
    Console.WriteLine("Bạn đoán " + guessCount + " lần");
  }
  
}